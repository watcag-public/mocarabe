int_iir8
int_iir8_33_2_700
results/int_iir8_33_2_700/solution1/syn/report/int_iir8_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_iir8, 33, 28116, 94576, 1485, 0, 3.225,
+ Latency: 
    * Summary: 
    +---------+---------+----------+----------+-----+-----+---------+
    |  Latency (cycles) |  Latency (absolute) |  Interval | Pipeline|
    |   min   |   max   |    min   |    max   | min | max |   Type  |
    +---------+---------+----------+----------+-----+-----+---------+
    |      131|      131| 0.187 us | 0.187 us |  131|  131|   none  |
    +---------+---------+----------+----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |      129|      129|        68|          2|          2|    31|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


