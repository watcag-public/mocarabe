int_caprasse
int_caprasse_405_4050_700
results/int_caprasse_405_4050_700/solution1/syn/report/int_caprasse_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_caprasse, 405, 47065, 235094, 4860, 0, 4.552,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       31|       31| 44.299 ns | 44.299 ns |   31|   31|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- Loop 1  |       29|       29|        20|          5|          5|     2|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


