int_caprasse
int_caprasse_405_5_700
results/int_caprasse_405_5_700/solution1/syn/report/int_caprasse_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_caprasse, 405, 56120, 257278, 4860, 0, 4.284,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       31|       31| 44.299 ns | 44.299 ns |   31|   31|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- Loop 1  |       29|       29|        20|          5|          5|     2|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


