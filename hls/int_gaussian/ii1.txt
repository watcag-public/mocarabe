adder_chain
int_gaussian_33_594_700
results/int_gaussian_33_594_700/solution1/syn/report/int_gaussian_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_gaussian, 33, 11170, 47262, 891, 0, 1.683,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       47|       47| 67.163 ns | 67.163 ns |   47|   47|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       45|       45|        15|          1|          1|    31|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


