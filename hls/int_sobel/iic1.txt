sobel
int_sobel_50_1_700
results/int_sobel_50_1_700/solution1/syn/report/int_sobel_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_sobel, 50, 8474, 27715, 150, 0, 1.628,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       34|       34| 48.586 ns | 48.586 ns |   34|   34|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       32|       32|        13|          1|          1|    20|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


