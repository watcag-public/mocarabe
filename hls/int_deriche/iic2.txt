int_deriche
int_deriche_14_2_700
results/int_deriche_14_2_700/solution1/syn/report/int_deriche_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_deriche, 14, 19248, 72684, 1344, 0, 2.460,
+ Latency: 
    * Summary: 
    +---------+---------+----------+----------+-----+-----+---------+
    |  Latency (cycles) |  Latency (absolute) |  Interval | Pipeline|
    |   min   |   max   |    min   |    max   | min | max |   Type  |
    +---------+---------+----------+----------+-----+-----+---------+
    |      187|      187| 0.267 us | 0.267 us |  187|  187|   none  |
    +---------+---------+----------+----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |      185|      185|        40|          2|          2|    73|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


