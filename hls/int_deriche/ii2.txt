int_deriche
int_deriche_14_560_700
results/int_deriche_14_560_700/solution1/syn/report/int_deriche_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_deriche, 14, 27695, 67656, 924, 0, 6.804,
+ Latency: 
    * Summary: 
    +---------+---------+----------+----------+-----+-----+---------+
    |  Latency (cycles) |  Latency (absolute) |  Interval | Pipeline|
    |   min   |   max   |    min   |    max   | min | max |   Type  |
    +---------+---------+----------+----------+-----+-----+---------+
    |      188|      188| 0.269 us | 0.269 us |  188|  188|   none  |
    +---------+---------+----------+----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |      186|      186|        43|          2|          2|    73|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


