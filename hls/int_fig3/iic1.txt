fig3
int_fig3_170_1_700
results/int_fig3_170_1_700/solution1/syn/report/int_fig3_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_fig3, 170, 14999, 42301, 510, 0, 1.448,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       18|       18| 25.722 ns | 25.722 ns |   18|   18|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       16|       16|        11|          1|          1|     6|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


