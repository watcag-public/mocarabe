int_fig3
int_fig3_306_1224_700
results/int_fig3_306_1224_700/solution1/syn/report/int_fig3_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_fig3, 306, 22951, 86628, 918, 0, 9.206,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       19|       19| 27.151 ns | 27.151 ns |   19|   19|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       17|       17|        12|          2|          2|     3|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


