int_bellido
int_bellido_207_828_700
results/int_bellido_207_828_700/solution1/syn/report/int_bellido_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_bellido, 207, 22496, 61315, 654, 0, 2.358,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       27|       27| 38.583 ns | 38.583 ns |   27|   27|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       25|       25|        14|          3|          2|     4|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


