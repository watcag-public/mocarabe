bellido
int_bellido_90_1_700
results/int_bellido_90_1_700/solution1/syn/report/int_bellido_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_bellido, 90, 8058, 27759, 810, 0, 1.997,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       24|       24| 34.296 ns | 34.296 ns |   24|   24|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       22|       22|        12|          1|          1|    11|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


