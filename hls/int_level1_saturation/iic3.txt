int_level1_saturation
int_level1_saturation_306_3_700
results/int_level1_saturation_306_3_700/solution1/syn/report/int_level1_saturation_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_level1_saturation, 306, 49347, 165453, 1836, 0, 6.505,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       31|       31| 44.299 ns | 44.299 ns |   31|   31|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       29|       29|        21|          3|          3|     3|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


