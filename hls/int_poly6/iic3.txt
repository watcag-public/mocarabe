int_poly6
int_poly6_306_3_700
results/int_poly6_306_3_700/solution1/syn/report/int_poly6_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_poly6, 306, 58433, 316819, 5508, 0, 4.680,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       60|       60| 85.740 ns | 85.740 ns |   60|   60|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       58|       58|        50|          3|          3|     3|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


