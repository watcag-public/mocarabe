int_approx1
int_approx1_405_5_700
results/int_approx1_405_5_700/solution1/syn/report/int_approx1_csynth.rpt
BENCH, UNROLL_FACTOR, LUTs, FF, DSP, BRAM,FREQ, Latency, II,
int_approx1, 405, 65943, 204047, 3645, 0, 3.860,
+ Latency: 
    * Summary: 
    +---------+---------+-----------+-----------+-----+-----+---------+
    |  Latency (cycles) |   Latency (absolute)  |  Interval | Pipeline|
    |   min   |   max   |    min    |    max    | min | max |   Type  |
    +---------+---------+-----------+-----------+-----+-----+---------+
    |       32|       32| 45.728 ns | 45.728 ns |   32|   32|   none  |
    +---------+---------+-----------+-----------+-----+-----+---------+

--
        |          |  Latency (cycles) | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min   |   max   |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+---------+---------+----------+-----------+-----------+------+----------+
        |- loop1   |       30|       30|        26|          5|          5|     2|    yes   |
        +----------+---------+---------+----------+-----------+-----------+------+----------+


